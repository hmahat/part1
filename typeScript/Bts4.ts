let arr: string[] = [
  "james",
  "Jack",
  "Brown",
  "Crowdway",
  "Singaham",
  "Eowday",
];

let newarr: string[] = [];

/* Copying items longer than 5 char  */
arr.forEach((item) => {
  if (item.length > 5) {
    newarr.push(item);
  }
});

console.log(newarr);
