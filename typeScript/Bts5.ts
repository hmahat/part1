let arrary: number[] = new Array(5);

arrary.fill(0);

let newarray = Array.from(
  arrary,
  (x) => Math.floor(Math.random() * 10) + x
);

console.log(newarray);

/* Sorting the array */

function compare(a: number, b: number): number {
  return b - a;
}
console.log(newarray.sort(compare));

/* Making reverese shorter */

console.log(newarray.sort((a: number, b: number) => a - b));
