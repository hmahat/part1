/* String Manipulation */
import readline from "readline-sync";

let str: string;
str = readline.question("Give me string : ");
let stringManipulation = (str: string): string => {
  return str.trim().toLowerCase().substr(0, 20);
};

console.log(stringManipulation(str));
