/* First non repeating character */
import readline from "readline-sync";
let input: string = readline.question("Input: ");

const nonRepChar = (input: string) => {
  let norep: string;
  for (let i = 0; i < input.length; i++) {
    let count: number = 0;
    for (let j = 0; j < input.length; j++) {
      if (input[i] === input[j]) {
        count += 1;
      }
    }
    if (count < 2) {
      norep = input[i];
      return norep;
    }
  }
};

console.log(nonRepChar(input));
